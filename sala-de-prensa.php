<link rel="stylesheet" href="/wp-content/themes/wp-bootstrap-starter/style.1.css" type="text/css"/>

<?php
/*
	Template Name: Sala de Prensa
*/
?>
<?php get_header(); ?>
<section id="primary" class="content-area px-0">
		<main id="main" class="site-main" role="main">
			<div id="post-<?php the_ID(); ?>" <?php post_class();?> style="margin-bottom:1em; margin-top:100px;">
                <?php if(has_post_thumbnail()){ echo '<div>';} else{echo '<div style="display: none">';}?>
                        <div class="post-thumbnail d-none d-md-block d-xl-none" style="display:inline-block; position:relative;   max-height: 400px;overflow: hidden;">
                                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
                        </div>
                        <div class="post-thumbnail d-none d-xl-block" style="display:inline-block; position:relative;  max-height:550px; overflow:hidden;">
                                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width:100%" />
                        </div>
                        <div class="post-thumbnail d-block d-md-none" style="width: 100%;">
                                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width:100%" />
                        </div>
        </div>
			<div class="row mx-0 mt-4" style="width: 100%">
				<div id="content" class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div class="mx-4 my-2 p-0">
						<h3 id="title_noticias">
									SALA DE PRENSA
						</h3>
						<br>
						<br>
						<div class="row pb-0 pt-5">

	<?php // Display blog posts on any page @ https://m0n.co/l
		//$temp = $wp_query; 
		//$wp_query= null;
		$wp_query = new WP_Query(array('category_name' => 'prensa', 'posts_per_page' => 6, '&paged='.$paged ));
		//$wp_query->query('posts_per_page=12' . '&paged='.$paged);
		$counter = 0;
		while ($wp_query->have_posts()) :
			 $wp_query->the_post(); ?>
			<?php if ($counter==3) {echo '</div><div class="row py-3">';}?>
				  <article class="col-12 col-md-4 pb-5" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			      <div class="post-thumbnail" style="text-align:justify;">
						<div>
						<?php if(has_post_thumbnail()){ echo '<div class="post-thumbnail fill" style="height: 10em;">';} else{echo '<div style="display: none">';}?>
							<img src="<?php echo the_post_thumbnail_url(); ?>" />
						</div>
				  		<div>
						<?php if(has_post_thumbnail()){ echo '<div style="display: none">';} else{echo '<div class="post-thumbnail fill" style="height: 10em;">';}?>
							<img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/08/noticias.jpg" style="width: 100%" />
						</div>
				  		<br>
				  		<header id="title_new" class="entry-header entry-header-1 max-lines-4">
							<?php
							if ( is_single() ) :
								the_title( '<h5 class="entry-title  font-weight-bold">', '</h5>' );
							else :
								the_title( '<h5 class="entry-title  font-weight-bold"><a id="ref" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h5>' );
							endif; ?>

					   </header><!-- .entry-header -->
				 </div>

				<div class="entry-content entry-content-1 max-lines-10" style="font-family: 'Source Sans Pro', sans-serif;">
						<?php the_excerpt(); ?>
				</div>
<style>

.sharedaddy{
	display: none !important;

}

.entry-content-1 p{
	color: #404040;
	display: inline;
	line-height: 1.2em !important;
	text-align:justify !important;
}


#title_new:hover{
	color:#769E30;
}

			#ref{
				text-decoration:none;
			}
			#ref:hover{
				color:#769E30;
			}

			#title_noticias{
				color:#769E30;
				text-align:center;
				font-family:sans-serif;
				font-weight:600;
			}
		</style>
							</article>
				<?php  $counter++; ?>
		<?php 
	 		  endwhile; ?>
						</div>
						<?php wp_reset_postdata(); ?>
					</div>
				</div>


<font size=7>
	<b style="font-family: 'Orbitron', sans-serif; h-100 ">
		<?php
		get_sidebar();
		echo '</div>';
		echo '</main>';
		echo '</section>';
		get_footer();?>
	</b>
</font>
