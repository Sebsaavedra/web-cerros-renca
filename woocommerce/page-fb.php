<link rel="stylesheet" href="/wp-content/themes/wp-bootstrap-starter/style.1.css" type="text/css"/>

<?php 
/*
	Template Name: FB
*/
?>
<!--
<?php 
	setlocale(LC_TIME,"es_ES");
?>
-->


<?php get_header(); ?>

	<section id="primary" class="content-area px-0">
		<main id="main" class="site-main" role="main">
			
            <!-- LANDING --> 
            <div id="post-<?php the_ID(); ?>" <?php post_class();?> style="margin-bottom: 1em">
				<?php if(has_post_thumbnail()){ echo '<div>';} else{echo '<div style="display: none">';}?>
					<div class="post-thumbnail d-none d-md-block" style="display: inline-block; position: relative; width: 100%; height: 400px;overflow: hidden;">
						<img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
					</div>
					<div class="post-thumbnail d-block d-md-none" style="width: 100%;">
						<img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
					</div>
				</div>
				<?php if(has_post_thumbnail()){ echo '<div style="display: none">';} else{echo '<div>';}?>
					<div class="post-thumbnail d-none d-md-block" style="display: inline-block; position: relative; width: 100%; height: 400px;overflow: hidden;">
						<img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/08/noticias-1024x400.jpg" style="width: 100%" />
						
					</div>
					<div class="post-thumbnail d-block d-md-none" style="width: 100%;">
						<img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/08/noticias-768x300.jpg" style="width: 100%" />
					</div>
				</div>	
			</div> 
            <!-- FIN LANDING --> 



            <!-- INICIO ROW INICIAL-->
			<div class="row mx-0 mt-4" style="width: 100%">
                <!-- INICIO CONTENEDOR EVENTOS TOTALES-->
				<div id="content" class="col-12">

                    <!-- INICIO DIV EVENTOS TOTALES-->
                    <div class="row" style="margin-right:20% !important;">
						<?php $events = tribe_get_events(); ?>
						<h2 style="margin-left:50%; font-family:Source Sans Pro, sans-serif; color:#769E33">PRÓXIMAS ACTIVIDADES</h2>
						 
                        
                        
                        <div>
                        
                            <?php get_events(); ?>
                        
                        </div>
                
						<?php if ($paged > 1) { ?>

						<nav id="nav-posts">
							<div class="prev"><?php next_posts_link('&laquo; Previous Posts'); ?></div>
							<div class="next"><?php previous_posts_link('Newer Posts &raquo;'); ?></div>
						</nav>

						<?php } else { ?>

						<nav id="nav-posts">
							<div class="prev"><?php next_posts_link('&laquo; Previous Posts'); ?></div>
						</nav>
						<?php } ?>
						<?php wp_reset_postdata(); ?>
					</div>
                    <!-- FIN DIV EVENTOS TOTALES-->
				</div>
                 <!-- FIN CONTENEDOR EVENTOS TOTALES-->   
            </div>
            <!-- FIN ROW INICIAL-->
            <br>
            <br>
                
<font size=7>
    <b style="font-family:'Orbitron', sans-serif; h-100" >
        <?php
            get_sidebar();
//            echo '</main>';
//            echo '</section>';
            get_footer();
        do_action( 'wp_footer' );
        ?>
    </b>
</font>
                