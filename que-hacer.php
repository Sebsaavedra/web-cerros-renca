<link rel="stylesheet" href="/wp-content/themes/wp-bootstrap-starter/style.1.css" type="text/css"/>
<link rel="stylesheet" id="fontawsome-css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css?ver=1.0.5"  type="text/css" media="all">




<?php 
/*
	Template Name: Que hacer
*/

get_header(); ?>



<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
	<section id="primary" class="content-area px-0 w-100">
		<main id="main" class="site-main" role="main">
			<div id="post-<?php the_ID(); ?>" <?php post_class();?> style="margin-bottom:0px; margin-top:100px;">
				<?php if(has_post_thumbnail()){ echo '<div>';} else{echo '<div style="display: none">';}?>
					<div class="post-thumbnail d-none d-md-block d-xl-none" style="display: inline-block; position: relative; width: 100%; max-height: 400px;overflow: hidden;">
						<img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />						
					</div>
					<div class="post-thumbnail d-none d-xl-block" style="display: inline-block; position: relative; width: 100%; max-height: 550px;overflow: hidden;">
						<img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
					</div>
					<div class="post-thumbnail d-block d-md-none" style="width: 100%;">
						<img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
					</div>
				</div>
				<?php if(has_post_thumbnail()){ echo '<div style="display: none">';} else{echo '<div>';}?>
					<div class="post-thumbnail d-none d-md-block" style="display: inline-block; position: relative; width: 100%; max-height: 400px;overflow: hidden;">
						<img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/10/que-hacer.jpg" style="width: 100%; h-75;" />	
					</div>
					<div class="post-thumbnail d-block d-md-none" style="width: 100%;">
						<img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/10/que-hacer.jpg" style="width: 100%" />
					</div>
				</div>
			</div><!-- #post-## -->


			<div class="card" style="background:linear-gradient(to right, rgba(164,179,87,1) 0%, rgba(117,137,12,1) 100%); ">
    <div style="width:100%">
            <div class="col-12">
                <div class="text-white"> 
                    <div class="card-body" style="font-family: 'Source Sans Pro', sans-serif;  text-align: justify;">
                    </div>
                    <div class="container">
                        <div class="row" style="width:100%;">
                            <div id="logo2" class="col-md-2" style="padding-right:0px;">
                                <img src="http://parquecerros.renca.cl/wp-content/uploads/2019/08/icono-que-hacer.svg" style="max-height: 90px">
                            </div>
                            <div id="title2" class="col-md-8" style="padding-left:0px;"><br>
                                <h3 align="left">
                                    <b style="font-family: 'Source Sans Pro', sans-serif;">PANORAMAS EN LOS CERROS DE RENCA</b>
                                </h3> 
                            </div>
                        </div>
                    </div>
              <p>
             </p>

<div class="container">
                <p id="text1" style="font-family: 'Source Sans Pro'; text-align: justify; line-height: 1.5; ">    
			El Parque Metropolitano Cerros de Renca es escenario de muchas actividades y distintas organizaciones sociales y comunitarias que trabajan en él. <br/><br/>
Si quieres estar informado o participar constantemente de actividades al aire libre, te recomendamos seguir a Renca Nativa y Trekking Tour Cerro Renca, dos organizaciones comunitarias que cuidan y promueven el patrimonio cultural y natural de nuestros cerros.
		</p>
              </div>
              <br>
              <br>
              <br>
              <br>
        	</div>
    </div>
</div>
</div>

<br>
<br>


<div id="socios" class="row" style="text-align:center; margin-left:15%; margin-right:15%;">
    <div class="col" style=" border-right: 1px solid black;">
      <img id="logo-1" src="http://parque.renca.cl/wp-content/themes/images/logos-05.png" style="max-width: 30%;  align="center">
    <h3 align="center" style="margin-top:20px;"><b style="font-family: 'Source Sans Pro', sans-serif; color: #769E30;">RENCA NATIVA</b></h3><br>

    <div class="row" style="display: inline-flex;">
	<a href="https://www.facebook.com/rencanativa"  target="_blank">
	   <div class="rs-facebook">
	        <div class="rs-centrar">
	        	<h4><i class="fa fa-facebook" aria-hidden="true"></i></h4>
	        </div>
	   </div>
	</a>
	<a href="https://instagram.com/renca_nativa" target="_blank">
	  <div class="rs-insta">
	      <div class="rs-centrar">
	           <h4><i class="fa fa-instagram" aria-hidden="true"></i></h4>
	      </div>
	  </div>
	</a>
     </div>



    </div>



    <div class="col">
      <img id="logo-2" src="http://parque.renca.cl/wp-content/themes/images/logos-06.png" style="max-width: 30%;  align="center">
      <h3 align="center" style="margin-top:20px;"><b style="font-family: 'Source Sans Pro', sans-serif; color: #769E30;">TREKKING TOUR CERRO RENCA</b></h3><br>
      <p align="center" style="color: gray; line-height: 1.4em;" >  Observación Eclipse Solar</p>

     <div class="row" style="display: inline-flex;">
	
	<a href="https://instagram.com/ttcerrorenca" target="_blank">
		<div class="rs-insta">
			<div class="rs-centrar">
	     			<h4><i class="fa fa-instagram" aria-hidden="true"></i></h4>
			</div>
		</div>
	</a>
     </div>
                                        <style>
                                            .fa {
                                                display: inline-block;
                                                font: normal normal normal 14px/1 FontAwesome;
                                                    font-size: 14px;
                                                font-size: inherit;
                                                text-rendering: auto;
                                                -webkit-font-smoothing: antialiased;
                                                -moz-osx-font-smoothing: grayscale;
                                            }
                                        </style>
    </div>


  </div>

  <br>
  <br>

  <div id="title_map_mobile" class="container"><br>
        <h3 align="center"><b style="text-align: center; font-family: 'Source Sans Pro', sans-serif; color: #769E30;">PLANIFICA TU VISITA</b></h3><br>
  </div>

  <div id="box_title_mapa" style="text-align:center; margin-left:10%; margin-right:10%; background: linear-gradient(to right, rgba(164,179,87,1) 0%, rgba(117,137,12,1) 100%);">
  	<b id="title_mapa" style="font-family: 'Source Sans Pro', sans-serif; color: #FFFFFF; font-size:23px; margin-left:5%; margin-right:5%;" >
      		HORARIO INVIERNO: Lunes a viernes: 08:30 a 19:00 horas Sábado, domingo y festivos: 09:00 a 19:00 horas
  	</b>
  </div>
</div>

  <div id="mapa_cerro" style="max-width: 100%; height:auto !important;">

 </div>
 <div class="row" style="width:100%; margin:0px;">
	<div class="post-thumbnail d-none d-md-block" style="display:inline-block; position:relative; height:100%; overflow:hidden; width:100%;">
		<img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2019/08/mapa-temporal.png" style="width:100%;" />	
	</div>
	<div class="post-thumbnail d-block d-md-none" >
		<img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2019/08/mapa-temporal.png" style="width:100%;" />
	</div>


 </div>
<br>
<br>


<style>


#box_title_mapa1{
	height:8%;
	padding-left:10%;
	padding-top:1%;
}

#box_title_mapa{
	padding-bottom:10px !important;
}

.rs-facebook:before, .rs-twitter:before, .rs-flickr:before, .rs-insta:before, .rs-youtube:before {

	box-shadow: 0 0 0 0 !important;
}


@media screen and (max-width: 1000px) {


	#mapa_cerro{
		margin-top:25% !important;
	}

	#img-que-hacer{
		width:100% !important;
	}

	#logo2{
        	width:50% !important;
		padding-top: 33px !important;
	}

	#title2{
        	width:50% !important;
		padding-right: 0px !important;
	}

	#logo-1{
		max-width:100% !important;
	}

	#logo-2{
		max-width:80% !important;
	}


}
</style>

<font size=7>
<b style="font-family: 'Orbitron', sans-serif; h-100">
		<?php

		echo '</main>';
		echo '</section>';
	
	 	get_footer();?>
	</b>
</font>
