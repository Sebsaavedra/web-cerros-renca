<link rel="stylesheet" href="/wp-content/themes/wp-bootstrap-starter/style.1.css" type="text/css"/>


<link rel="stylesheet" id="fontawsome-css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css?ver=1.0.5"  type="text/css" media="all">


<?php 
/*
	Template Name: Agenda
*/
?>
<!--
<?php 
	setlocale(LC_TIME,"es_ES");
?>
-->


<?php get_header(); ?>

	<section id="primary" class="content-area px-0">
		<main id="main" class="site-main" role="main">
			
	<div id="post-<?php the_ID(); ?>" <?php post_class();?> style="margin-bottom:1em; margin-top:100px;">
                <?php if(has_post_thumbnail()){ echo '<div>';} else{echo '<div style="display: none">';}?>
                        <div class="post-thumbnail d-none d-md-block d-xl-none" style="display:inline-block; position:relative;   max-height: 400px;overflow: hidden;">
                                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
                        </div>
                        <div class="post-thumbnail d-none d-xl-block" style="display:inline-block; position:relative;  max-height:550px; overflow:hidden;">
                                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width:100%" />
                        </div>
                        <div class="post-thumbnail d-block d-md-none" style="width: 100%;">
                                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width:100%" />
                        </div>
        </div>

            <!-- INICIO ROW INICIAL-->
			<div class="row mx-0 mt-4" style="width: 100%">
                <!-- INICIO CONTENEDOR EVENTOS TOTALES-->
				<div id="content" class="col-12">
                     <br>
                     <br>
                    <!-- INICIO DIV EVENTOS TOTALES-->
                    <div class="row" > 
						<h3 style="text-align: center;  width:600px;">
						  <b style="font-family:'Source Sans Pro', sans-serif; color:#769E33;">PRÓXIMAS ACTIVIDADES</b>
                         </h3>
                        <div style="width:100%;">

                            <?php
                            // Retrieve the next 5 upcoming events
                        $events = tribe_get_events( array(
                            'posts_per_page' => 5,
			    'order' => 'DESC',
                        ) );

                        // Loop through the events, displaying the title
                        // and content for each
                        foreach ( $events as $event ) {
                        ?>
                        <br>
                            <br>
                           <!--  INICIO DIV SI HAY EVENTOS -->
							<div class="row" style="margin-left:10%; margin-right:10%; ">
                                 <div class="col-12 col-md-2 font-orange-red">
                                            <div class="row">
                                                <div class="col-12 text-md-center">
                                                    <h5 class="mb-0" style="font-family:Source Sans Pro, sans-serif;"><?php echo tribe_get_start_date($event, true, "D"); ?></h5>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 text-md-center">
                                                    <h1 class="mb-0 font-weight-bold" style="font-family:Source Sans Pro, sans-serif;"><?php echo tribe_get_start_date($event, true, "d"); ?></h1>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 text-md-center">
                                                    <h5 style="font-family:Source Sans Pro, sans-serif;"><?php echo tribe_get_start_date($event, true, "M y"); ?></h5>
                                                </div>
                                            </div>
								</div>

								<div id="contenedor_noticias" class="col-9 col-md-8 px-4">
										<h5 class="font-weight-bold font-black link-orange-red">

						<a style="font-weight:bold;  color:#d75e24; text-decoration:none !important; " href="<?php tribe_event_link($event); ?>"><?php echo $event->post_title; ?> </a>
					    
					    <?php 
					  
                                            echo tribe_events_get_the_excerpt($event);
                                            ?></h5>
                                    <p class="mb-1 font-grey text-size-90"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo tribe_get_start_date($event, true, "h:i A"); ?></p>
									<p class="mb-0 font-grey text-size-90"><i style="padding-left:5px;" class="fa fa-map-marker" aria-hidden="true"></i> <?php echo tribe_get_address($event); ?></p>
								</div>

								<div class="col-3 col-md-2 px-4">
										<div class="row">
											<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo tribe_get_event_link($event)?>" target="_blank">
										        <div class="rs-facebook-2">
										            <div class="rs-centrar">
										                <h4><i class="fa fa-facebook" aria-hidden="true"></i></h4>
										            </div>
										        </div>
										        </a>
										        <a href="https://twitter.com/home?status=<?php echo tribe_get_event_link($event)?>" target="_blank">
										        <div class="rs-twitter-2">
										            <div class="rs-centrar">
										                <h4><i class="fa fa-twitter" aria-hidden="true"></i></h4>
										            </div>
										        </div>
										        </a>
										</div>
                                        <style>
                                            .fa {
                                                display: inline-block;
                                                font: normal normal normal 14px/1 FontAwesome ;
                                                 font-size: 12px;
                                                font-size: inherit;
                                                text-rendering: auto;
                                                -webkit-font-smoothing: antialiased;
                                                -moz-osx-font-smoothing: grayscale;
                                            }
                                        </style>
								</div>
                                
                                 <style>
                                        #contenedor_noticias p{
                                             color: rgb(64, 64, 64) !important;
                                             line-height: 1.2 !important;
                                        }
                                    </style>
							</div>
                            <br>
                            <br>
                            <!--  FIN DIV SI HAY EVENTOS -->

                            <?php
                            }
                        ?>
 
                        </div>

						<?php if ($paged > 1) { ?>

						<nav id="nav-posts">
							<div class="prev"><?php next_posts_link('&laquo; Previous Posts'); ?></div>
							<div class="next"><?php previous_posts_link('Newer Posts &raquo;'); ?></div>
						</nav>

						<?php } else { ?>

						<nav id="nav-posts">
							<div class="prev"><?php next_posts_link('&laquo; Previous Posts'); ?></div>
						</nav>
						<?php } ?>
						<?php wp_reset_postdata(); ?>
					</div>
                    <!-- FIN DIV EVENTOS TOTALES-->
				</div>
                 <!-- FIN CONTENEDOR EVENTOS TOTALES-->   
            </div>
            <!-- FIN ROW INICIAL-->
            <br>
            <br>
                
<font size=7>
    <b style="font-family:'Orbitron', sans-serif; h-100" >
        <?php
            get_sidebar();
//            echo '</main>';
//            echo '</section>';
            get_footer();
        do_action( 'wp_footer' );
        ?>
    </b>
</font>
                
