<link rel="stylesheet" href="/wp-content/themes/wp-bootstrap-starter/style.1.css" type="text/css"/>


<?php
/**
* Template Name: Contacto
 */

get_header(); ?>

	<section id="primary" class="content-area col-sm-12">
		<main id="main" class="site-main" role="main">
            <div class="row" style="margin-top:90px;">
            
            <div class="container" style="padding-top:20px;">
							<h3 align="center"><b style="font-family: 'Source Sans Pro', sans-serif; color: #769E30;">CONTÁCTANOS</b></h3><br>
			</div> 
                
            <?php echo do_shortcode( '[contact-form-7 id="314" title="Contacto"]' ); ?>
            
            
            </div>
		</main><!-- #main -->
	</section><!-- #primary -->

<style>

    #wpcf7-f314-o1{
        margin-left: 25% !important;
        width: 50% !important;
    }


    .wpcf7-form-control.wpcf7-textarea.form-control{
     margin-top: 1.5em;
     border-color:#769E30 !important;
     width: 100%;
     padding: 1em;
     @include border-radius(8px);
     display: block;
    }

   
    input{  
     border-color:#769E30 !important;
     border:0; outline:0;
     padding: 1em;
     @include border-radius(8px);
     display: block;
     width: 100%;
     margin-top: 1em;
     font-family: 'Merriweather', sans-serif;
     @include box-shadow: 0px 1px 30px 8px rgba(41,255,66,1);
     resize: none;
    
        &:focus {
           @include box-shadow: 0px 1px 30px 8px rgba(41,255,66,1);
        }
     } 
    
    .wpcf7-not-valid-tip{
        float: right;
        padding:10px !important;
    }
    
    #boton_enviar{
        background-color:#769E30
        
    }
    
    label{
    color:#769E30 !important;
    font-weight:bold !important;
    }

	     


    .btn-primary{
        background-color:#769E30 !important; 
        border-color:#769E30 !important;
    }

</style>

<font size=7>
	<b style="font-family: 'Orbitron', sans-serif; h-100 ">
		<?php
		get_footer();?>
	</b>
</font>

