<link rel="stylesheet" id="fontawsome-css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css?ver=1.0.5"  type="text/css" media="all">
    <link rel="stylesheet" href="/wp-content/themes/wp-bootstrap-starter/style.1.css" type="text/css"/>



<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class();?> style="margin-bottom: 1em">

	<br>
	<br>
	
	<div class="post-thumbnail d-none d-md-block" style="display:inline-block; position:relative; width:100%; max-height:600px; overflow:hidden;">

	</div>

</div><!-- #post-## -->
<div style="width: 100%">
	<div id="content">
		<div>
		<header class="entry-header">
			<h1 id="titulo_noticia" class="entry-title">
				<?php the_title(); ?>
			</h1>
		<br>
		<br>
		<div id="contenido_noticia" class="entry-content" style="text-align: justify;">
			<?php
	        if ( is_single() ) :
				the_content();
	        else :
	            the_content( __( 'Leer más<span class="meta-nav">&rarr;</span>', 'wp-bootstrap-starter' ) );
	        endif;


			?>
		</div>
			
		<div>
		  	<?php
		while ( have_posts() ) : the_post();
			
			    the_post_navigation();

			echo '</div>';
			echo '</div>';

		endwhile; // End of the loop.
		?>
		</div>	
		
	<!-- .entry-content -->

			
			<style>
				#titulo_noticia{
					font-family:'Source Sans pro';
					font-weight:700;
					size:2.5rem;
					line-height:1.2;
					color:#769E30;
					 margin-right:5%;
                                        margin-left:5%;

				}
			
				#contenido_noticia{
					//font-family:'';
					font-weight:400;
					font-size: 1rem;
					line-height:1.5;
					color:#404040;
					white-space: pre-line;
					 margin-right:5%;
                                        margin-left:5%;


	
				}
				
				h3, h2, h1, h4, h5, h6, p, span, b{
					font-family:'Source Sans pro';
				}




			</style>
<font size=7>
    <b style="font-family:'Orbitron', sans-serif; h-100" >
        <?php
            get_sidebar();
//            echo '</main>';
//            echo '</section>';
            get_footer();
        do_action( 'wp_footer' );
        ?>
    </b>
</font>
