/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class();?> style="margin-bottom: 1em">
	<?php if(has_post_thumbnail()){ echo '<div>';} else{echo '<div style="display: none">';}?>
		<div class="post-thumbnail d-none d-md-block d-xl-none" style="display: inline-block; position: relative; width: 100%; max-height: 400px;overflow: hidden;">
			<img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
			
		</div>
		<div class="post-thumbnail d-none d-xl-block" style="display: inline-block; position: relative; width: 100%; max-height: 550px;overflow: hidden;">
			<img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
			
		</div>
		<div class="post-thumbnail d-block d-md-none" style="width: 100%;">
			<img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
		</div>
	</div>
	<?php if(has_post_thumbnail()){ echo '<div style="display: none">';} else{echo '<div>';}?>
		<div class="post-thumbnail d-none d-md-block" style="display: inline-block; position: relative; width: 100%; height: 400px;overflow: hidden;">
			<img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/themes/www_renca/images/page-default.png" style="width: 100%" />
			
		</div>
		<div class="post-thumbnail d-block d-md-none" style="width: 100%;">
			<img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/themes/www_renca/images/page-default.png" style="width: 100%" />
		</div>
	</div>
	<div class="row" style="width: 100%; margin-left: 0; margin-top: -8px">
		<div class="col-2" style="height: 8px; background-color: #d75e24;"></div>
		<div class="col-2" style="height: 8px; background-color: #e68e0c;"></div>
		<div class="col-2" style="height: 8px; background-color: #769e33;"></div>
		<div class="col-2" style="height: 8px; background-color: #009a88;"></div>
		<div class="col-2" style="height: 8px; background-color: #5fb9d3;"></div>
		<div class="col-2" style="height: 8px; background-color: #0192d0;"></div>
	</div>
</div><!-- #post-## -->

<div class="row mt-3 mx-0 mb-0" style="width: 100%">
	<div id="content" class="col-sm-12 col-md-12 col-lg-8 col-xl-9">
		<div class="mx-2 my-4 m-sm-4 m-md-5 px-4">
			
			
		<header class="entry-header">
			<?php
			if ( is_single() ) :
				the_title( '<h1 style="font-weight: bold;" class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;

?>

		</header><!-- .entry-header -->
			
			
		<div class="entry-content">
	
			<?php
	        if ( is_single() ) :
				the_content();
	        else :
	            the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'wp-bootstrap-starter' ) );
	        endif;

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wp-bootstrap-starter' ),
					'after'  => '</div>',
				) );
			?>
		</div>
		
	<!-- .entry-content -->


	<footer class="entry-footer">
		<?php wp_bootstrap_starter_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
