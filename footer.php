
<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>

<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
    <?php get_template_part( 'footer-widget' ); ?>
    <!-- Footer -->


<div id="footer-mobile" style=" width:100%; display:none;  background: linear-gradient(to right, rgba(164,179,87,1) 0%, rgba(117,137,12,1) 100%);">
	<div class="row" style="padding-top:5%; margin-right:0px;">
                        <a href="http://parque.renca.cl/">
                                <img src="http://parque.renca.cl/wp-content/uploads/2018/07/LOGO-cerros-de-renca-blanco.png" style="margin-left:40%; margin-top:10%; max-height:120px">
                        </a>
        </div>

	<div class="row" style="margin-right:0px; text-align:center; margin-left:5%; margin:rigth:5%; padding-top:6%;">
                        <p>     <b style="font-size:19px;"><i>Verano:</i></b> Lunes a viernes: 08:30 a 20:00 horas
                                                        Sábado, domingo y festivos: 09:00 a 19:00 horas<br><br>
                                                        <b><i>Invierno: </i></b> Lunes a viernes: 08:30 a 19:00 horas
                                                        Sábado, domingo y festivos: 09:00 a 19:00 horas<br>
                        </p>
                        <p style="text-align:center; margin-left:4%; margin:rigth:4%;"> <span>©  </span><span class="copyright-year">2018</span>&nbsp; <span>Parque Metropolitano Cerros de Renca</span> </p>
        </div>

         <div class="row" style="margin-right:0px;  width:100%; padding-left:10%;">
              <div class="column">
                   <a href="http://parque.renca.cl/">
                      <img style="width:100%;" src="http://parque.renca.cl/wp-content/uploads/2019/08/logo-renca.svg">
                   </a>
               </div>

               <div class="column">
             	    <a href="http://parque.renca.cl/"> 
                	<img style="width:100%;" src="http://parque.renca.cl/wp-content/uploads/2019/08/nuevo-escudo.svg">
                    </a>
               </div>

               <div class="column">
                   <a href="http://parque.renca.cl/">
                        <img style="width:65%;" src="http://parque.renca.cl/wp-content/uploads/2018/11/logo-scam-21.png">
                   </a>
               </div>
        </div>

	<div class="row" style="margin-right:0px; width:100%; padding-left:10%; padding-right:10%; padding-top:4%;">
	<div class="row-icon">
		<a href="https://www.facebook.com/MuniRenca/" target="_blank"><i class="fab fa-facebook-square" style="color:white;"></i>
		</a>
	</div>	
	<div class="row-icon">
	  <a  href="https://twitter.com/Muni_Renca" target="_blank"><i class="fab fa-twitter" style="color:white"></i>
	  </a>
	</div>
	<div class="row-icon">
		<a  href="https://www.instagram.com/muni_renca/" target="_blank"><i class="fab fa-instagram" style="color:white"></i>
		</a>
	</div>
	<div class="row-icon">
	    <a  href="https://www.flickr.com/photos/153772620@N02" target="_blank"><i class="fab fa-flickr" style="color:white"></i>
		</a>
	</div>
	<div class="row-icon">
		<a  href="https://www.youtube.com/channel/UCosdtBKfRSxhhi4x9247Nbw" target="_blank"><i class="fab fa-youtube" style="color:white"></i>
		</a>
	</div>
	</div>

	<div class="row" style="padding-left:10%; padding-top:4%; margin-right:0px;  ">
		<p class="text-white" style="font-family: 'Source Sans Pro', sans-serif !important; font-size:16px; letter-spacing:3px;"><b>Información:contacto@renca.cl</b></p> 
	</div>



</div>


    <div id="footer-web" style="background: linear-gradient(to right, rgba(164,179,87,1) 0%, rgba(117,137,12,1) 100%);">
      <div class="row" style="margin-left:0; margin-right:0px; height:40%; background: linear-gradient(to right, rgba(164,179,87,1) 0%, rgba(117,137,12,1) 100%);  align:center">
 	<div class="footer-1">
			<a href="http://parque.renca.cl/">
				<img src="http://parque.renca.cl/wp-content/uploads/2018/07/LOGO-cerros-de-renca-blanco.png" style="max-height:120px" alt="Parque Metropolitano Cerros de 								Renca">
					</a>
	</div>

	<div class="footer-2">
			<strong style="font-size:16px; color:white;">Dirección:</strong><p class="text-white" style="margin-bottom:0px !important;"> Av. El Cerro 1555, Renca</p>
			<strong style="font-size:16px; color:white;">Horarios:</strong>
			<p style=" line-height:1.4; font-size:16px;">	<b style="font-size:16px; "><i>Verano:</i></b> Lunes a viernes: 08:30 a 20:00 horas
							Sábado, domingo y festivos: 09:00 a 19:00 horas<br>
							<b><i>Invierno: </i></b> Lunes a viernes: 08:30 a 19:00 horas
							Sábado, domingo y festivos: 09:00 a 19:00 horas<br>


			</p>
			<p> <span>©  </span><span class="copyright-year">2018</span>&nbsp; <span>Parque Metropolitano Cerros de Renca</span> </p>
	</div>

	<div class="footer-space">
        </div>

	<div class="footer-3" style="display:flex;">
   		<div class="column">
			<a href="http://parque.renca.cl/">
		  		<img style="width:100%;" src="http://parque.renca.cl/wp-content/uploads/2019/08/logo-renca.svg">
			</a>
   		</div>

  		<div class="column">
			<a href="http://parque.renca.cl/">
				<img style="width:100%;" src="http://parque.renca.cl/wp-content/uploads/2019/08/nuevo-escudo.svg">
        		</a>
  		</div>

  		<div class="column">
        		<a href="http://parque.renca.cl/">
               			<img style="width:55%;" src="http://parque.renca.cl/wp-content/uploads/2018/11/logo-scam-21.png">
        		</a>
  		</div>
 	</div>
		<div class="footer-space">
                </div>

	         <div class="footer-4">
			<!-- TERCERA COLUMNA FOOTER -->
					<div class="column-icon">
						<a style="width:50%;"  href="https://www.facebook.com/MuniRenca/" target="_blank"><i class="fab fa-facebook-square" style="color:white;"></i>
						</a>
					</div>
					<div class="column-icon">
					  <a href="https://twitter.com/Muni_Renca" target="_blank"><i class="fab fa-twitter" style="color:white"></i>
					  </a>
					</div>
					<div class="column-icon">
						<a href="https://www.instagram.com/muni_renca/" target="_blank"><i class="fab fa-instagram" style="color:white"></i>
						</a>
					</div>
					<div class="column-icon">
					    <a href="https://www.flickr.com/photos/153772620@N02" target="_blank"><i class="fab fa-flickr" style="color:white"></i>
						</a>
					</div>
					<div class="column-icon">
						<a href="https://www.youtube.com/channel/UCosdtBKfRSxhhi4x9247Nbw" target="_blank"><i class="fab fa-youtube" style="color:white"></i>
						</a>
					</div>
		<br>
		<div style="width:300px;">
	     <p class="text-white" style="font-family: 'Source Sans Pro', sans-serif !important; font-size:16px; letter-spacing:3px;"><b>Información:contacto@renca.cl</b></p> 
		</div>
   			    <!-- FIN TERCERA COLUMNA FOOTER -->
      </div>
</div>
</div>
<?php endif; ?>





<style>
.footer-1{
  float: left;
  width: 15% !important;
  padding: 10px;
 font:200 !important;
 font-family: 'Source Sans Pro', sans-serif !important;
 align-self:center !important; 
 margin-left:50px !important;
}



.footer-2{
  float: left;
  width: 20% !important;
  padding: 10px;
  font:200 !important;
  font-family: 'Source Sans Pro', sans-serif !important;
  align-self:center !important;
  
}



.footer-3{
  float: left;
  width: 30% !important;
  padding: 10px;
  font-family: 'Source Sans Pro', sans-serif !important;
  align-self:center !important;
}

.footer-space{
	width:4% !important;
}



.footer-4{
  float: left;
  width: 30% important;
  padding: 10px;
 font:200 !important;
 font-family: 'Source Sans Pro', sans-serif !important;
align-self:center !important;


}

.column {
  float: left;
  width: 33.33%;
  padding: 5px;
  align-self: center !important;
}

.column-icon {
  float: left;
  width: 20%;
  padding: 2px;
  align-self: center !important;
}


.row:after {
  content: "";
  display: table;
  clear: both;
}


/* VERSION MOBILE  */
@media screen and (max-width: 1000px){


#footer-web{
	display:none !important;
}

#footer-mobile{
	display:block !important;
}



.row-icon {
  float: left;
  width: 20%;
  padding: 2px;
  align-self: center !important;

}


}



</style>
<?php wp_footer();
echo '</body>';
echo '</html>';?>


