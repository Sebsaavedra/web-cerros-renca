<link rel="stylesheet" href="/wp-content/themes/wp-bootstrap-starter/style.1.css" type="text/css"/>

<?php
/**
* Template Name: Proceso Participativo
 */

get_header(); ?>


		<main id="main" class="site-main" role="main">

<div id="post-<?php the_ID(); ?>" <?php post_class();?> style="margin-bottom:1em; margin-top:100px;">
                <?php if(has_post_thumbnail()){ echo '<div>';} else{echo '<div style="display: none">';}?>
                        <div class="post-thumbnail d-none d-md-block d-xl-none" style="display:inline-block; position:relative;   max-height: 400px;overflow: hidden;">
                                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
                        </div>
                        <div class="post-thumbnail d-none d-xl-block" style="display:inline-block; position:relative;  max-height:550px; overflow:hidden;">
                                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width:100%" />
                        </div>
                        <div class="post-thumbnail d-block d-md-none" style="width: 100%;">
                                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width:100%" />
                        </div>
        </div>

<div  style="width: 100%">
				<div id="content" class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style="font-family: 'Source Sans Pro', sans-serif; color: #000000;">

						<div style="font-family: 'Source Sans Pro', sans-serif; color: #000000;">							
							<div class="col align-self-center">

<div id="post-384" class="post-384 post type-post status-publish format-standard has-post-thumbnail hentry category-noticias" style="margin-bottom: 1em">

	<br>
	<br>

	<div class="post-thumbnail d-none d-md-block" style="display:inline-block; position:relative; width:100%; max-height:600px; overflow:hidden;">
 
	</div>

</div><!-- #post-## -->
<div id="box-general" style="width: 100%">
	<div id="content" class="col-sm-12 col-md-12 col-lg-8 col-xl-9">
		<div id="box-noticia" style="margin-right:1.5em; margin-left:1.5em;">
		<header>
			<h3 id="title-1"style="text-transform: uppercase; font-weight:600; text-align: justify; font-family: sans-serif; color:#769E30;   ">
				Así hemos vivido el proceso participativo del Parque Metropolitano Cerros de Renca			</h3>
		<br>
		<br>
		<div style="text-align: justify;" class="entry-content">
		Desde marzo del año 2018 como Municipalidad de Renca, junto a Fundación de Urbanismo Social, llevamos adelante un proceso de participación para definir de la mano de la comunidad cuál será el Plan Maestro del Parque Metropolitano Cerros de Renca. <br/><br/>
Para esta tarea se ha invitado a participar al arquitecto Teodoro Fernández y el equipo de Lyon Bosch + Martic. Estos destacados profesionales recogerán los resultados de los distintas instancias participativas realizadas con la comunidad para entregar la carta de navegación que contendrá los proyectos detonantes del parque de aquí a 30 años.<br/><br/>
El proceso aún no acaba, todavía puedes contestar las consultas que haremos en nuestras redes sociales y participar de la fiesta de cierre en la que se entregará el Plan Maestro ¡No te quedes fuera, súbete al cerro!


		</div>
<br>
<br>

<h3 id="title-2" style="font-weight:600; text-align: justify; font-family: sans-serif; color:#769E30;  ">EL PARQUE METROPOLITANO CERROS DE RENCA SE CREA CON PARTICIPACIÓN</h3>
<br>
<div><p style="font-weight:bold; text-align:justify; font-family: 'Source Sans Pro', sans-serif; color:gray; ">Revisa todas las etapas de este proceso a continuación:</p></div>


<br>
<div >
	<img id="myImg" src="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-768x768.jpg" align="middle" data-attachment-id="385" data-permalink="http://parque.renca.cl/asi-hemos-vivido-el-proceso-participativo-del-parque-metropolitano-cerros-de-renca/participacion-en-renca-02/" data-orig-file="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02.jpg" data-orig-size="2118,2118" data-comments-opened="0" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="participacion en Renca-02" data-image-description="" data-medium-file="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-300x300.jpg" data-large-file="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-1024x1024.jpg" class="alignnone wp-image-385 size-large jetpack-lazy-image jetpack-lazy-image--handled" src="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-1024x1024.jpg" alt="" data-lazy-loaded="1" srcset="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-1024x1024.jpg 1024w, http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-150x150.jpg 150w, http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-300x300.jpg 300w, http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-768x768.jpg 768w" sizes="(max-width: 1024px) 100vw, 1024px" width="1024" height="1024"><noscript><img data-attachment-id="385" data-permalink="http://parque.renca.cl/asi-hemos-vivido-el-proceso-participativo-del-parque-metropolitano-cerros-de-renca/participacion-en-renca-02/" data-orig-file="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02.jpg" data-orig-size="2118,2118" data-comments-opened="0" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="participacion en Renca-02" data-image-description="" data-medium-file="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-300x300.jpg" data-large-file="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-1024x1024.jpg" class="alignnone wp-image-385 size-large" src="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-1024x1024.jpg" alt="" width="1024" height="1024" srcset="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-1024x1024.jpg 1024w, http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-150x150.jpg 150w, http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-300x300.jpg 300w, http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-768x768.jpg 768w" sizes="(max-width: 1024px) 100vw, 1024px" /></noscript></div>
		</header></div>	</div>	</div>

	<!-- The Modal -->
<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>


<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>






<style>

#box-general{
	padding-left:15%;
}



@media screen and (max-width: 1000px){
	#contenido_noticia{
		font-family:'Arial';
		font-weight:400;
		font-size: 1rem;
		line-height:1.5;
		color:#404040;
	}

	#title-1{
		text-align: center !important;
	}

	#title-2{
		text-align: center !important;
	}
}







@media screen and (max-width:370px){

	#box-general{
		padding-left:0px !important;

	}

	#content{
		padding-left:0px !important;
		padding-right:0px !important;
	}


/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation */
.modal-content, #caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }















}

</style>
		         	         	</div>
						</div>
				</div>
</div></div></main>




<font size=7>
	<b style="font-family: 'Orbitron', sans-serif; h-100 ">
		<?php
		get_footer();?>
	</b>
</font>
