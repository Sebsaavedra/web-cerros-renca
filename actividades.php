<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>


<?php
/*
cd &var&www
	Template Name: Actividades
*/
?>
<?php get_header(); ?>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
	<section id="primary" class="content-area px-0 w-100" >
		<main id="main" class="site-main" role="main">
			<div id="post-<?php the_ID(); ?>" <?php post_class();?> style="margin-bottom: 1em; margin-top:100px;">
				<?php if(has_post_thumbnail()){ echo '<div>';} else{echo '<div style="display: none">';}?>
					<div class="post-thumbnail d-none d-md-block d-xl-none" style="display: inline-block; position: relative; width: 100%; max-height: 400px;overflow: hidden;">
						<img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
					</div>
					<div class="post-thumbnail d-none d-xl-block" style="display: inline-block; position: relative; width: 100%; max-height: 550px;overflow: hidden;">
						<img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
					</div>
					<div class="post-thumbnail d-block d-md-none" style="width: 100%;">
						<img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
					</div>
				</div>
				<?php if(has_post_thumbnail()){ echo '<div style="display: none">';} else{echo '<div>';}?>
					<div class="post-thumbnail d-none d-md-block" style="display: inline-block; position: relative; width: 100%; max-height: 400px;overflow: hidden;">
						<img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/08/actividades.jpg" style="width: 100%; h-75;" />
					</div>
					<div class="post-thumbnail d-block d-md-none" style="width: 100%;">
						<img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/08/actividades.jpg" style="width: 100%" />
					</div>
				</div>
			</div><!-- #post-## -->


<div id="actividades" class="container" style="padding-top:100px;">
					<h3 align="center"><b style="font-family: 'Source Sans Pro', sans-serif; color: #769E30;">GALERÍA</b></h3><br>
</div>


<?php
  $currCat = get_category(get_query_var('actividades'));
  $cat_name = $currCat->name;
  $cat_id   = get_cat_ID( 'Actividades' );
?>

<?php
  $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
  $temp = $wp_query;
  $wp_query = null;
  $wp_query = new WP_Query();
  $wp_query->query('showposts=2&post_type=post&paged='.$paged.'&cat='.$cat_id);
  while ($wp_query->have_posts()) : $wp_query->the_post();
?>

 <div>
            <div>
                <div><br/>
                    <div id="contenedor-actividades">
                            <div class="col-md-8" style="font-family: 'Source Sans Pro', sans-serif;">
   			    <div style="text-transform: uppercase; color:#769E30;  font-family: 'Source Sans Pro', sans-serif;">
                            	<?php echo  '<h4>' . get_the_title() . '</h4>'  ; ?>
                            </div>
	                        <?php echo '<p style="text-align: justify; color: black; font-family:Source Sans Pro, sans-serif; font-weight:400; line-height:1.4em"><br/>' . get_the_content() .'</p>' ; ?>
				 <?php echo '<p style="font-family:Source Sans Pro, sans-serif; font-weight:400; line-height:1.2em"><br/>' .  do_shortcode(get_the_excerpt()) ; ?>
				<br>
				<div align="right">	

					<?php
				   $pattern1 = get_the_excerpt();
                        echo '<br>';
                        $str_arr1 = explode("=", $pattern1);
                        $str_nuevo1 = explode("m", $str_arr1[1]);
                        $cod1 = substr($str_nuevo1[0],1,-3);
                        $url1 = 'https://www.flickr.com/photos/munirenca/albums/'.$cod1;
                    echo '<a id="boton-1" class="btn btn-primary; btn btn-outline-light justify-content-end" style="font-size: 18px;  color:#769E30; " href="'.$url1.'" role="button">';
                    echo '<b id="texto-1" style="text-align: center; font-family: "Source Sans Pro", sans-serif; font-size: 18px;">Ver más en flickr...</b>';
                    echo '</a>';
                  ?>
 				</div>
                            </div> 
                            <br/>
                    </div> 
                    <br/>
                </div> 
            </div> 
            <br/>
	   </div>






<?php endwhile; ?>



<?php
  global $wp_query;
 
  $big = 999999999; // need an unlikely integer
  echo '<div id="contenedor-nav" class="paginate-links">';
    echo paginate_links( array(
    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format' => '?paged=%#%',
    'prev_text' => __('<'),
    'next_text' => __('>'),
    'current' => max( 1, get_query_var('paged') ),
    'total' => $wp_query->max_num_pages
    ) );
  echo '</div>';
?>






<style>

.justified-gallery>a {
pointer-events: none;
cursor: default;
}



#contenedor-actividades{
	margin-left:27%;
}


#contenedor-nav{
	margin-left:45%;
	margin-bottom:5%; 
}


.page-numbers {
	display: inline-block;
	padding: 10px 15px 10px 15px;
	color: gray;
	border: 1px solid #eee;
	line-height: 1;
	text-decoration: none !important;
	border-radius: 2px;
	font-weight: 600;
	font-size: 15px !important;
}
.page-numbers.current,
a.page-numbers:hover {
	background: #769E30;
	color:white;
}



a.prev.page-numbers:hover {
        background: #769E30;
        color:white;
}

a.prev.page-numbers{
        background: gray;
        border-radius:50px !important;
           display: inline-block;
        padding: 3px 12px 9px 9px;
        border: 1px solid #eee;
        line-height: 1;
        text-decoration: none !important;
        font-weight: 600;
        font-size: 31px !important;
        color: white;
}

a.next.page-numbers:hover {
        background: #769E30;
        color:white;
}

a.next.page-numbers{
	background: gray;
	border-radius:50px !important;
	   display: inline-block;
        padding: 3px 12px 9px 9px;
        border: 1px solid #eee;
        line-height: 1;
        text-decoration: none !important;
        font-weight: 600;
        font-size:31px !important;
	color: white;
}


.btn{


color: #769E30; !important;
  font-size: 20px;
  font-weight: 500;
  padding: 0.5em 1.2em;
  background: #769E30;
  border: 2px solid;
  border-color: #769E30;
  position: relative;

}



#boton-1{
color: #769E30; !important;
  font-size: 20px;
  font-weight: 500;
  padding: 0.5em 1.2em;
  background: rgba(0,0,0,0);
  border: 2px solid;
  border-color: #769E30;
  transition: all 1s ease;
  position: relative;
}

#boton-1:hover {
  background: #769E30;
  color: #fff !important;
}





#proceso-participativo{

	margin-left:-46px !important;


}

#agenda{

	margin-left:-49px !important;

}


.main{
	width:100%;
}




@media screen and (max-width:500px){

#contenedor-actividades{

	margin-left:0%;
}


#contenedor-nav{
        margin-left:16%;
        margin-bottom:5%;
}




}




</style>



<font size=7>
	<b style="font-family: 'Orbitron', sans-serif; h-100">
	<?php
get_sidebar();
echo '</div>';
echo '</main>';
echo '</section>';
get_footer();?>
	</b>
</font>

