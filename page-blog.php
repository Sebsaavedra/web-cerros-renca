<?php 
/*
	Template Name: Blog
*/
?>
<?php get_header(); ?>

	<section id="primary" class="content-area px-0">
		<main id="main" class="site-main" role="main">
			<div id="post-<?php the_ID(); ?>" <?php post_class();?> style="margin-bottom: 1em">
				<?php if(has_post_thumbnail()){ echo '<div>';} else{echo '<div style="display: none">';}?>
					<div class="post-thumbnail d-none d-md-block" style="display: inline-block; position: relative; width: 100%; height: 400px;overflow: hidden;">
						<img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
						
					</div>
					<div class="post-thumbnail d-block d-md-none" style="width: 100%;">
						<img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
					</div>
				</div>
				<?php if(has_post_thumbnail()){ echo '<div style="display: none">';} else{echo '<div>';}?>
					<div class="post-thumbnail d-none d-md-block" style="display: inline-block; position: relative; width: 100%; height: 400px;overflow: hidden;">
						<img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/themes/www_renca/images/page-default.png" style="width: 100%" />
						
					</div>
					<div class="post-thumbnail d-block d-md-none" style="width: 100%;">
						<img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/themes/www_renca/images/page-default.png" style="width: 100%" />
					</div>
				</div>
				<div class="row" style="width: 100%; margin-left: 0; margin-top: -8px">
					<div class="col-2" style="height: 8px; background-color: #d75e24;"></div>
					<div class="col-2" style="height: 8px; background-color: #e68e0c;"></div>
					<div class="col-2" style="height: 8px; background-color: #769e33;"></div>
					<div class="col-2" style="height: 8px; background-color: #009a88;"></div>
					<div class="col-2" style="height: 8px; background-color: #5fb9d3;"></div>
					<div class="col-2" style="height: 8px; background-color: #0192d0;"></div>
				</div>
			</div><!-- #post-## -->
			<div class="row mx-0 mt-3" style="width: 100%">
				<div id="content" class="col-sm-12 col-md-12 col-lg-8 col-xl-9">
					<div class="mx-4 my-2 p-4">
						<h2>NOTICIAS</h2>
						<div class="row pb-0 pt-5">
							<?php // Display blog posts on any page @ https://m0n.co/l
							$temp = $wp_query; $wp_query= null;
							$wp_query = new WP_Query(); $wp_query->query('posts_per_page=9' . '&paged='.$paged);
							$counter = 0;
							while ($wp_query->have_posts()) : 
								$wp_query->the_post(); 
								?>
								<?php 
								if ($counter==3) {echo '</div><div class="row py-3">';}?>
									



							<article class="col-12 col-md-4 pb-5" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								<div class="post-thumbnail">
									<div>
										<?php if(has_post_thumbnail()){ echo '<div class="post-thumbnail fill" style="height: 10em;">';} else{echo '<div style="display: none">';}?>
											<img src="<?php echo the_post_thumbnail_url(); ?>" />
										</div>
										<?php if(has_post_thumbnail()){ echo '<div style="display: none">';} else{echo '<div class="post-thumbnail fill" style="height: 10em;">';}?>
											<img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/themes/www_renca/images/placeholder.jpg" style="width: 100%" />
										</div>
			
									</div>
								</div>
								<br>
								<header class="entry-header entry-header-1 max-lines-4">
									<?php
									if ( is_single() ) :
										the_title( '<h5 class="entry-title link-blue font-weight-bold">', '</h5>' );
									else :
										the_title( '<h5 class="entry-title link-blue font-weight-bold"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h5>' );
									endif; ?>

								</header><!-- .entry-header -->
								<div class="entry-content entry-content-1 max-lines-10">
									<?php the_excerpt(); ?>
								</div>
								
							</article><!-- #post-## -->

							<?php 
							$counter++;
							endwhile; ?>
						</div>

						

						<nav id="nav-posts">
							<div class="text-center pagination"><?php echo paginate_links( array('prev_text' => __('<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>'), 'next_text' => __('<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>')) ); ?></div>
						</nav>

						

						<?php wp_reset_postdata(); ?>
					</div>
				</div>


<?php
get_sidebar();
echo '</div>';
echo '</main>';
echo '</section>';
get_footer();
