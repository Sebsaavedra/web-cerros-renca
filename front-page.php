    <link rel="stylesheet" href="/wp-content/themes/wp-bootstrap-starter/style.1.css" type="text/css"/>


    <?php
    /**
     * The front page template file
     *
     * If the user has selected a static page for their homepage, this is what will
     * appear.
     * Learn more: https://codex.wordpress.org/Template_Hierarchy
     *
     * @package WordPress
     * @subpackage Twenty_Seventeen
     * @since 1.0
     * @version 1.0
     */

    get_header(); ?>
    <body onload="carga()">
    <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
        <div id="primary" class="site-content">

        </div><!-- #primary -->

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.11&appId=1841972096058534';
            fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>






    <section id="video">
        <!--
    	<div id="video_div" class="v-container">
    		<iframe width="560" height="215" src="https://www.youtube.com/embed/KiH6GLyLH0A?version=3&loop=1&autoplay=1&cc_load_policy=1rel=0&amp;controls=0&amp;showinfo=0&playlist=KiH6GLyLH0A&mute=1" frameborder="0" allow="autoplay; encrypted-media;" allowfullscreen></iframe>
    	</div>


	    <div id="contador" style="background: linear-gradient(to right, rgba(164,179,87,1) 0%, rgba(117,137,12,1) 100%);  z-index:200;">

        <h6 class="text-white" align="center" style="font-family: 'Source Sans Pro', sans-serif; padding-top:4%;">YA VAMOS EN:</h6>
               <div class="container">
                  <div class="row justify-content-center">
                          <div><p style="color:white; font-family: 'digital-clock-font'; font-size:70px;  display:inline !important;">6.625</p>
                          </div>
                 </div>
	<h6 class="text-white" align="center" style="font-family: 'Source Sans Pro', sans-serif; padding-top:1%;">ÁRBOLES PARA RENCA</h6>


                </div>
        -->
        <div id="donation-banner" class="v-container" style="background-image:url('http://parquecerros.renca.cl/wp-content/uploads/2019/11/home.png'); background-size:cover;  background-repeat: no-repeat;">
            <div class="row" style="padding-top: 15%;">
                <div class="col-8 m-auto text-center text-white">
                    <img src="http://parquecerros.renca.cl/wp-content/themes/images/home-txt.svg" class="img-fluid mb-4">
                    <a href="http://parquecerros.renca.cl/adopta-un-arbol/">
                        <img src="http://parquecerros.renca.cl/wp-content/themes/images/home-btn.svg" width="400px">
                    </a>
                </div>
            </div>
        </div>
    </div>



     </section>

    <style>
    @media screen and (max-width:600px) {
        #video{
            z-index:0; !important;
        }

        #video_div{
            z-index:0; !important;
        }

      }

	@font-face{
 	font-family:'digital-clock-font';
 	src: url('wp-content/uploads/2019/08/digital-7.ttf');
	}

	#contador {
  	position: absolute;
  	top: 170px;
  	right: 100px;
	width:220;
	height:130;
	}


    </style>


    <section id="sd" class="content-area px-0 mw-100">
            <main id="main" class="site-main" role="main"></main>


    <?php
    // The Query
   $the_query = new WP_Query(array('category_name' => 'Destacada'));
         $count=0;
         $band=0;
         $uno=1;
         $dos=2;
     if ( $the_query->have_posts() ) {
          $count = $the_query->post_count;
          $array = array();// The Loop
          for ($i = 1; $i <= $count; $i++) {
              $the_query->the_post();
              $address_post_id = get_the_ID() ;
              $array[$band] = $address_post_id;
              $penultimo = $count - $count;
              $ultimo = $count - $count+1;
	      $band ++;
	  }
    ?>



    <section id="bloques_web" style="margin:0;">

      <!-- fila 1 -->
      <div class="row">
        <div id="box_text_web_1"  style="max-width:50%;  background: linear-gradient(to right, rgba(255,175,75,1) 0%, rgba(214,88,39,1) 100%);">
          <div class="text-white"><br/>
            <div class="row justify-content-center">
              <div class="col-md-8" style="font-family: 'Source Sans Pro', sans-serif;">
                <div syle="font-family: 'Source Sans Pro', sans-serif;">
         	        <?php echo  '<h4>' . get_the_title($array[$penultimo]) . '</h4>'  ; ?>
                </div>
                <?php echo '<p style="text-align:justify; font-family:Source Sans Pro, sans-serif; font-weight:400; line-height:1.2em"><br/>' . get_the_excerpt($array[$penultimo]) .'</p>' ; ?>
                <div align="right">
           	      <?php
                 		echo '<a class="btn btn-primary; btn btn-outline-light justify-content-end" style="font-size: 16px;" href=' . get_permalink($array[$penultimo]) . ' role="button">';
                 		echo '<b style="text-align: center; font-family: "Source Sans Pro", sans-serif; font-size: 16px;">Ver más...</b>';
                 		echo '</a>';
          	      ?>
                </div>
              </div>
              <br/>
            </div>
            <br/>
          </div>
        </div>

        <?php
          $url1 = wp_get_attachment_url( get_post_thumbnail_id($array[$penultimo]) );
          echo'<div id="box_img_web_1" style="width:50%; height: auto;  background-image:url(' . $url1 . '); background-size:cover; background-position:center;  background-repeat: no-repeat; ">';
          echo '</div>';
        ?>

        <div id="box_text_mobile_1" class="col-md-12" style=" display:none; background: linear-gradient(to right, rgba(255,175,75,1) 0%, rgba(214,88,39,1) 100%);">
          <div class="text-white"><br/>
            <div class="row justify-content-center">
              <div class="col-md-8" style="font-family: 'Source Sans Pro', sans-serif;">
                <div style="font-family: 'Source Sans Pro', sans-serif;">
            	   <?php echo  '<h4>' . get_the_title($array[$penultimo]) . '</h4>'  ; ?>
                </div>
                <?php echo '<p style="text-align:justify; font-family:Source Sans Pro, sans-serif; font-weight:400; line-height:1.2em"><br/>' . get_the_excerpt($array[$penultimo]) .'</p>' ; ?>
                <div align="right">
                  <?php
                   	echo '<a class="btn btn-primary; btn btn-outline-light justify-content-end" style="font-size: 16px;" href=' . get_permalink($array[$penultimo]) . ' role="button">';
                   	echo	'<b style="text-align: center; font-family: "Source Sans Pro", sans-serif; font-size: 16px;">Ver más...</b>';
                   	echo	'</a>';
                  ?>
                </div>
              </div>
              <br/>
            </div>
            <br/>
          </div>
        </div>

        <div style="width:100%; height:auto;">
          <?php
          	$url2 = wp_get_attachment_url( get_post_thumbnail_id($array[$penultimo]) );
          	echo '<div id="box_img_mobile_1"  style="width:100%; height:400px; display:none; background-image:url(' . $url2 . '); background-size:cover; background-position:center;  background-repeat: no-repeat; ">';
          	echo '</div>';
          ?>
        </div>
      </div>



      <!-- fila 2 -->
      <div class="row">
        <?php
          $url1 = wp_get_attachment_url( get_post_thumbnail_id($array[$ultimo]) );
          echo'<div id="box_img_web_2" style="width:50%; height: auto;  background-image:url(' . $url1 . '); background-size:cover; background-position:center;  background-repeat: no-repeat; ">';
          echo '</div>';
        ?>

        <div id="box_text_web_2"  style="max-width:50%; background: linear-gradient(to right, rgba(164,179,87,1) 0%, rgba(117,137,12,1) 100%);  ">
          <div class="text-white"><br/>
            <div class="row justify-content-center">
              <div class="col-md-8" style="font-family: 'Source Sans Pro', sans-serif;">
                <div syle="font-family: 'Source Sans Pro', sans-serif;">
         	        <?php echo  '<h4>' . get_the_title($array[$ultimo]) . '</h4>'  ; ?>
                </div>
                <?php echo '<p style="text-align:justify; font-family:Source Sans Pro, sans-serif; font-weight:400; line-height:1.2em"><br/>' . get_the_excerpt($array[$ultimo]) .'</p>' ; ?>
                <div align="right">
               	  <?php
                 		echo '<a class="btn btn-primary; btn btn-outline-light justify-content-end" style="font-size: 16px;" href=' . get_permalink($array[$ultimo]) . ' role="button">';
                 		echo	'<b style="text-align: center; font-family: "Source Sans Pro", sans-serif; font-size: 16px;">Ver más...</b>';
                 		echo	'</a>';
              	  ?>
                </div>
              </div>
              <br/>
            </div>
            <br/>
          </div>
        </div>


        <div id="box_text_mobile_2" class="col-md-12" style=" display:none; background: linear-gradient(to right, rgba(164,179,87,1) 0%, rgba(117,137,12,1) 100%); ">
          <div class="text-white"><br/>
            <div class="row justify-content-center">
              <div class="col-md-8" style="font-family: 'Source Sans Pro', sans-serif;">
                <div style="font-family: 'Source Sans Pro', sans-serif;">
              	 <?php echo  '<h4>' . get_the_title($array[$ultimo]) . '</h4>'  ; ?>
                </div>
                <?php echo '<p style="text-align:justify; font-family:Source Sans Pro, sans-serif; font-weight:400; line-height:1.2em"><br/>' . get_the_excerpt($array[$ultimo]) .'</p>' ; ?>
                <div align="right">
                  <?php
                   	echo '<a class="btn btn-primary; btn btn-outline-light justify-content-end" style="font-size: 16px;" href=' . get_permalink($array[$ultimo]) . ' role="button">';
                   	echo	'<b style="text-align: center; font-family: "Source Sans Pro", sans-serif; font-size: 16px;">Ver más...</b>';
                   	echo	'</a>';
                  ?>
                </div>
              </div>
              <br/>
            </div>
            <br/>
          </div>
        </div>

        <div style="width:100%; height:auto;">
          <?php
          	$url2 = wp_get_attachment_url( get_post_thumbnail_id($array[$ultimo]) );
          	echo '<div id="box_img_mobile_2"  style="width:100%; height:400px; display:none; background-image:url(' . $url2 . '); background-size:cover; background-position:center;  background-repeat: no-repeat; ">';
          	echo '</div>';
          ?>
        </div>
      </div>

      <!-- fila 3 -->
      <div class="row" style="width:100%; height:250px;">
        <div id="box_img_web_3" style="width:50%; height: auto; background-image: url('wp-content/uploads/2018/08/el-proyecto-down.jpg');  background-size:cover;  background-position: 30% 80%; background-repeat: no-repeat;">
          <div class="text-white"><br/>
            <div class="row justify-content-center">
              <div class="col-md-10" style="font-family: 'Source Sans Pro', sans-serif; " >
                <h3 style="font-family:Source Sans Pro, sans-serif; font-weight:bold; ">REVISA LA AGENDA</h3>
                <h3  style="font-family:Source Sans Pro, sans-serif; font-weight:bold; ">DE PANORAMAS</h3>
                <div align="left">
                  <?php
                    echo '<a class="btn btn-primary; btn btn-outline-light justify-content-end" style="font-size: 16px;" href="http://parquecerros.renca.cl/agenda/" role="button">';
                    echo '<b style="text-align: center; font-family: "Source Sans Pro", sans-serif; font-size: 16px;">Ver más...</b>';
                    echo '</a>';
                  ?>
                </div>
              </div>
              <br/>
            </div>
            <br/>
          </div>
        </div>

        <div id="box_img_mobile_3" style="width:100%; height:200px; display:none; background-image: url('wp-content/uploads/2018/08/el-proyecto-down.jpg');  background-size:cover;  background-position: 30% 80%; background-repeat: no-repeat;">
          <div class="text-white"><br/>
            <div class="row justify-content-center">
              <div class="col-md-10" style="font-family: 'Source Sans Pro', sans-serif; " >
                <h3 style="font-family:Source Sans Pro, sans-serif; font-weight:bold; ">REVISA LA AGENDA</h3>
                <h3  style="font-family:Source Sans Pro, sans-serif; font-weight:bold; ">DE PANORAMAS</h3>
                <div align="left">
                  <?php
                    echo '<a class="btn btn-primary; btn btn-outline-light justify-content-end" style="font-size: 16px;" href="http://parquecerros.renca.cl/agenda/" role="button">';
                    echo '<b style="text-align: center; font-family: "Source Sans Pro", sans-serif; font-size: 16px;">Ver más...</b>';
                    echo '</a>';
                  ?>
                </div>
              </div>
              <br/>
            </div>
            <br/>
          </div>
        </div>


        <div id="box_img_web_4" style="width:50%; height:250px; background-image: url('wp-content/themes/images/renca-participa.svg');  background-size: cover; background-position: 10% 20%; background-repeat: no-repeat;">
          <div class="text-white"><br/>
            <div class="row justify-content-center">
              <div class="col-md-10" style="font-family: 'Source Sans Pro', sans-serif; " >
              </div>
            </div>
            <br/>
          </div>
          <br/>
        </div>


        <div id="box_img_mobile_4" style="width:100%; height:400px; display:none; background-image: url('wp-content/themes/images/renca-participa.svg');  background-size: cover; background-position: 10% 20%; background-repeat: no-repeat;">
          <div class="text-white"><br/>
            <div class="row justify-content-center">
              <div class="col-md-10" style="font-family: 'Source Sans Pro', sans-serif; " >
              </div>
            </div>
            <br/>
          </div>
          <br/>
        </div>
      </div>

    <?php
     }
    ?>
    </section>

  <br>
  <div id="title_map_web" class="container"><br>
  	<h3 align="center"><b style="text-align: center; font-family: 'Source Sans Pro', sans-serif; color: #769E30;">PARQUE METROPOLITANO CERROS DE RENCA</b></h3><br>
  </div>

  <div id="title_map_mobile" class="container"  style="display:none;"><br>
        <h3 align="center"><b style="text-align: center; font-family: 'Source Sans Pro', sans-serif; color: #769E30;">MAPA CERROS DE RENCA</b></h3><br>
  </div>

  <div id="box_title_mapa" style="text-align:center; margin-left:10%; margin-right:10%; background: linear-gradient(to right, rgba(164,179,87,1) 0%, rgba(117,137,12,1) 100%);">
  	<b id="title_mapa" style="font-family: 'Source Sans Pro', sans-serif; color: #FFFFFF; font-size:23px; margin-left:5%; margin-right:5%;" >
      		HORARIO INVIERNO: Lunes a viernes: 08:30 a 19:00 horas Sábado, domingo y festivos: 09:00 a 19:00 horas
  	</b>
  </div>


  <div id="mapa_cerro" style="width: 100%;">
    <div class="bg-info text-white" style="width:100%;">
        <a href="<?php echo esc_url( home_url( '/' )); ?>">
          <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2019/08/mapa-temporal.png" tyle="background-image:url" align="right" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
        </a>
    </div>
  </div>
 <br>
 <br>
 <div id="titulo_colaboradores"><br><br>
 	<h3 align="left"><b style="font-family: 'Source Sans Pro', sans-serif; color: #769E30;">COLABORADORES</b></h3><br>
 </div>
 <div class="row" id="colaboradores">
	<div id="col_cultiva" class="col-md-3"><a href="http://parque.renca.cl/">
	<img src="http://parque.renca.cl/wp-content/uploads/2018/08/logos-socios-cultiva-1.png" style="max-width: 200%; max-height: 80%;  " align="right" alt="Parque Metropolitano Cerros de Renca">
	</a>
 </div>
 <div id="col_teodoro" class="col-md-3"><a href="http://parque.renca.cl/">
 	<img src="http://parque.renca.cl/wp-content/uploads/2018/08/logos-socios-atquitectos-1.png" style="max-width: 100%; max-height: 100%; margin-top:35px; margin-left:60px;"  alt="Parque Metropolitano Cerros de Renca">
	</a>
 </div>
 <div id="col_lbm" class="col-md-3"><a href="http://parque.renca.cl/">
	<img src="http://parque.renca.cl/wp-content/uploads/2018/08/logos-socios-lbm-2.png" style="max-height: 100%; margin-top:-20px; margin-left:100px;"  alt="Parque Metropolitano Cerros de Renca">
	</a>
 </div>

 <div id="col_urba" class="col-md-3" style="position: left;"><a href="http://parque.renca.cl/">
	<img align="left" src="http://parque.renca.cl/wp-content/uploads/2018/08/logos-socios-urbanismo-social.png" style="max-height: 80%; margin-top:10px;">
	</a>
 </div>

 </div



</section>
<br>
<br>







<style>
/* VERSION FULL WIDTH */

#donation-banner {
    background-position:0 100px;
}

    #box_title_mapa{
	//height:6% !important;
    	padding-top:10px !important;
	padding-bottom:10px !important;
    }
    .row{
	margin-left:0px !important;
        margin-right:0px !important;
    }

    #box_title_mapa1{
	padding-top:25px;
	margin-top:0px !important;
   }

    #box_title_cerro{
	margin-top:10px !important;
    }

	#titulo_colaboradores{
		padding-left:8%;
	}


 #titulo_colaboradores h3{
	padding-top:50px !important;
}


/* VERSION NOTEBOOK 14 */
@media screen and (min-width: 1001px) and (max-width: 1600px) {
	#title_mapa{
        	margin-left:5% !important;
    	}

	#box_title_mapa{

	}

	#box_title_mapa1{
		height:20% !important;
		margin-left:15% !important;
	}

	#box_title_cerro{
		margin-top:30px !important;
	}
}

/* VERSION MOBILE  */
@media screen and (max-width: 1000px){
    #donation-banner {
        background-position:0 0;
    }

	#title_map_web{
		display: none !important;
	}

	#bloques_web{
		margin-bottom:20% !important;
	}

	#contador{
		display:none !important;
	}
	#title_map_mobile{
		display: inline-block !important;
	}

	#box_img_mobile_4{
        	display: inline-block !important;
		height:150px !important;
	}

	#box_img_web_4{
        	visibility:hidden !important;
        	display:none !important;
	}

	#box_img_mobile_3{
        	display: inline-block !important;
	}

	#box_img_web_3{
       	 	visibility:hidden !important;
        	display:none !important;
	}

	#box_text_mobile_1{
		display:inline-block !important;
	}

	#box_text_web_1{
		display: none !important;
	}

	#box_text_mobile_2{
                display:inline-block !important;
        }

        #box_text_web_2{
                display: none !important;
        }

	#box_title_mapa1{
		visibility:hidden !important;
		visible:none !important;
	}

	#box_img_web_1{
		visibility:hidden !important;
        	display:none !important;
	}

	#box_img_web_2{
		visibility:hidden !important;
		display:none !important;
	}

	#mapa_cerro{
		margin-top:25% !important;
	}

	#box_title_cerro{
		margin-top:120px !important;
		padding-top:150px !important;
	}

	#box_title_mapa{
		margin-top:0 !important;
		height:auto !important;
	}

	#box_img_mobile_1{
		display: inline-block !important;
	}

	#box_img_mobile_2{
                display:inline-block !important;
        }

        #contenedor_front{
            padding:0px !important;
        }

        #icon1{
            width:20% !important;
        }

        #icon2{
            width:20% !important;
        }

        #titulo_colaboradores h3{
        	text-align:center !important;
	}

	#col_cultiva{
		padding-left:40% !important;
		margin-left:30px !important;
		width:70% !important;
	}
	#col_teodoro{
		width:70% !important;
		margin-left:1% !important;
	}
	#col_lbm{
		width:70% !important;
		padding-left:13% !important;
		padding-top:40px !important;
	}
	#col_urba{
		width:70% !important;
		margin-left:19% !important;
		padding-top:22px !important;
	}

        #tabla_footer{
            display:none !important;
        }
}
</style>



<font size=7>
    <b style="font-family: 'Orbitron', sans-serif; h-100 ">
    <?php
        get_sidebar();
        get_footer();
    ?>
    </b>
</font>
