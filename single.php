<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */
?>




<?php remove_filter('the_content', 'wpautop'); ?>
<?php get_header(); ?>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
	<section id="primary" class="content-area px-0">
		<main id="main" class="site-main" role="main">
			<div id="post-<?php the_ID(); ?>" <?php post_class();?> style="margin-bottom:1em; margin-top:100px;">
                <?php if(has_post_thumbnail()){ echo '<div>';} else{echo '<div style="display: none">';}?>
                        <div class="post-thumbnail d-none d-md-block d-xl-none" style="display:inline-block; position:relative;   max-height: 400px;overflow: hidden;">
                                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
                        </div>
                        <div class="post-thumbnail d-none d-xl-block" style="display:inline-block; position:relative;  max-height:550px; overflow:hidden;">
                                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width:100%" />
                        </div>
                        <div class="post-thumbnail d-block d-md-none" style="width: 100%;">
                                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width:100%" />
                        </div>
        </div>


			</div><!-- #post-## -->
			<div  style="width: 100%">
				<div id="content" style="font-family: 'Source Sans Pro', sans-serif; color: #000000;">

						<div  style="font-family: 'Source Sans Pro', sans-serif; color: #000000;">	
							<div class="col align-self-center">
								<?php get_template_part( 'template-parts/content', 'page'); ?>
	         	         	</div>
						</div>
				</div>
<?php
echo '</div>';
echo '</main>';
echo '</section>';
get_footer();
