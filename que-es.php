<link rel="stylesheet" href="/wp-content/themes/wp-bootstrap-starter/style.1.css" type="text/css"/>
<?php
/*
    Template Name: Que es
*/
?>
<?php get_header(); ?>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<div id="primary" style="width:100%; margin-top: 170px;">
    <div class="card" style="background-color:white; border:0 !important;">
        <div style="width:100%">
            <div class="col-12">
                <div class="text-white">
                    <div class="card-body" style="font-family: 'Source Sans Pro', sans-serif;  text-align: justify;">
                    </div>
                    <div class="container mb-5">
                        <div class="row" style="width:100%">
                            <div id="logo2" class="col-2">
                                <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/themes/images/icono-parque-10.svg" style="color: #769E30; max-height: 80px">
                            </div>
                            <div id="title2" class="col-10">
                                <h1 align="left" style="font-family: 'Source Sans Pro', sans-serif; color: #769E30; font-weight: bolder; font-size: 4rem; line-height:3rem;">
                                    30.000 ÁRBOLES
                                </h1>
                                <h3 align="left" style="font-family: 'Source Sans Pro', sans-serif; color: #769E30; font-style: italic; font-weight: normal;">
                                    Para el Parque Metropolitano Cerros de Renca
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="container mb-5">
                        <p id="text1"  style="font-family: 'Source Sans Pro'; text-align: justify; line-height: 1.5; color:black; ">
                            Como Municipalidad de Renca, junto a Cultiva y Avina, hemos formado una alianza que llama a la acción en torno a la realidad inminente de la crisis climática global.
                        </p>
                        <p id="text1"  style="font-family: 'Source Sans Pro'; text-align: justify; line-height: 1.5; color:black; ">
                            <b>El día 6 de Diciembre desarrollaremos un gran hito de acción climática: la reforestación de 15.000 árboles nativos en el Parque Metropolitano Cerros de Renca, para luego plantar otros 15.000 durante el 2020.</b>
                            Este plan de reforestación es parte del proyecto de recuperación de este espacio para convertir a los Cerros de Renca en el mayor pulmón verde de la zona norponiente de Santiago.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card" style="text-align:justify; background: linear-gradient(to right, rgba(255,175,75,1) 0%, rgba(214,88,39,1) 100%);">
        <div style="width:100%">
            <div class="col-12">
                <div class="text-white">
                    <div class="card-body" style="font-family: 'Source Sans Pro', sans-serif;  text-align: center;">

                    </div>
                    <div class="container mb-5">
                        <div class="row" style="width:100%;">
                            <div id="title1" class="col-12 text-center">
                                <h3 style="font-family: 'Source Sans Pro', sans-serif; font-weight: bolder;">
                                    ¡SÚMATE ADOPTANDO 1 Ó MÁS ÁRBOLES NATIVOS!
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="container mb-5">
                        <div class="row" class="text-center" style="width:100%;">
                            <div class="col-4">
                                <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=CNDZFCR9MX9EL&source=url">
                                    <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/themes/images/donar1.svg" style="max-height: 200px">
                                </a>
                            </div>
                            <div class="col-4" style="border-left: 1px solid white; border-right: 1px solid white;">
                                <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6PM72E6DFWEN4&source=url">
                                    <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/themes/images/donar2.svg" style="max-height: 200px">
                                </a>
                            </div>
                            <div class="col-4">
                                <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=97DSD67A8282Y&source=url">
                                    <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/themes/images/donar3.svg" style="max-height: 200px">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="post-thumbnail" style="display:inline-block; position:relative; width:100%; max-height:500px; overflow:hidden; width:100%;">
        <div class="col-6 m-auto">
            <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/themes/images/logos.svg" style="width:100%;" />
        </div>
    </div>
    <div class="post-thumbnail d-none d-md-block" style="display:inline-block; position:relative; width:100%; max-height:500px; overflow:hidden; width:100%;">
        <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2019/08/footer-el-parque.png" style="width:100%;" />
    </div>
    <div class="post-thumbnail d-block d-md-none" >
        <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2019/08/footer-el-parque.png" style="width:100%;" />
    </div>
</div>

<style>
/* VERSION FULL WIDTH */
#post-151{
    margin-bottom:0px !important;
}

#proyectos{
    margin-left:30px;
    margin-right:30px;
}
#title1{

}



/* VERSION MOBILE */
@media screen and (max-width: 1000px) {

#logo1{
    width:50% !important;
}
#title1{
    width:50% !important;
}

#logo2{
        width:50% !important;
}
#title2{
        width:50% !important;
}


#logo3{
        width:50% !important;
}
#title3{
        width:50% !important;
}

#proyectos{
    display:none !important;
}


#proyectos2{
    display:inline-block ! important;
    margin-left:5% !important;
    margin-right: 5% !important;
}

#titulo_proyecto_1, #titulo_proyecto_2, #titulo_proyecto_3, #titulo_proyecto_4 {
    font-size:initial !important;
}




#los-proyectos{
    margin-left:0px !important;
}

}
</style>


<font size=7>
    <b style="font-family: 'Orbitron', sans-serif; h-100 ">
    <?php
        get_sidebar();
        get_footer(); ?>
    </b>
</font>
